package br.com.cursojava.designpatterns.singleton;

public class SingletonTest {

	public static void main(String[] args) {
		Singleton s, b, c, d, e, f;
		
		s = Singleton.getInstance();
		b = Singleton.getInstance();
		c = Singleton.getInstance();
		d = Singleton.getInstance();
		e = Singleton.getInstance();
		f = Singleton.getInstance();
		s.print();
		b.print();
		c.print();
		d.print();
		e.print();
		f.print();
	}
	
}
