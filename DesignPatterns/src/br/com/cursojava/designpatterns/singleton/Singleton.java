package br.com.cursojava.designpatterns.singleton;

public class Singleton {

	private static Singleton instance; 
	
	private Singleton() {
		super();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Singleton getInstance() {
		if (null == instance) {
			instance = new Singleton();
		}
		return instance;
	}
	
	public void print(){
		System.out.println("Terminei a cria��o, j� estou apto a ser utilizado");
	}
}
