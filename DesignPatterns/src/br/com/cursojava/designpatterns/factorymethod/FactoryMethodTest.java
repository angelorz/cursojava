package br.com.cursojava.designpatterns.factorymethod;

public class FactoryMethodTest {

	public static void main(String[] args) {
		if ("1".equals(args[0])){
			testFamilyA();
		} else {
			testFamilyB();
		}
	}

	private static void testFamilyA() {
		FactoryMethod factory; 
		Product p;
		
		factory = new FactoryMethodFamilyA();
		
		p = factory.create();
		System.out.println(p.getValor());
	}
	
	private static void testFamilyB() {
		FactoryMethod factory; 
		Product p;
		
		factory = new FactoryMethodFamilyB();
		
		p = factory.create();
		System.out.println(p.getValor());
	}
	
}
