package br.com.cursojava.designpatterns.factorymethod;

public class FactoryMethodFamilyB implements FactoryMethod {

	@Override
	public Product create() {
		return new ProductFamilyB();
	}

}
