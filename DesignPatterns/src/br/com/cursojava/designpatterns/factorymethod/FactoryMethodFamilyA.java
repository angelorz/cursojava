package br.com.cursojava.designpatterns.factorymethod;

public class FactoryMethodFamilyA implements FactoryMethod {

	@Override
	public Product create() {
		return new ProductFamilyA();
	}

}
