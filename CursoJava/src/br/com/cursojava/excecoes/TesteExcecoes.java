package br.com.cursojava.excecoes;

import javax.swing.plaf.basic.BasicTreeUI.TreeHomeAction;

import br.com.cursojava.excecoes.MinhaPrimeiraException;
import br.com.cursojava.excecoes.MinhaSegundaException;
import br.com.cursojava.excecoes.MinhaTerceiraException;

public class TesteExcecoes {

	
	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * 
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 */
	public static void testaPrimeiraException(Integer inteiro) throws MinhaPrimeiraException {
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
		}
	}
	
	
	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * 
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 */
	public static void testaHierarquiaDaPrimeiraException(Integer inteiro) throws Exception {
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
		}
	
		if (inteiro % 9 == 0) {
			throw new MinhaTerceiraException();
		}
		
		
	}
	
	
	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * 
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 * @throws MinhaSegundaException 
	 */
	public static void testaTodasExceptions(Integer inteiro) throws MinhaPrimeiraException, MinhaSegundaException {
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException("Exce��o pois o numero � multiplo de 5");
		}
		
		if (inteiro % 9 == 0) {
			throw new MinhaTerceiraException();
		}
		
		if (inteiro % 13 == 0) {
			throw new MinhaSegundaException();
		}
	}
	
	
	public static void main(String[] args){

//  Bloco para teste da minha primeira exception
//		try {
//			testaPrimeiraException(2);
//			testaPrimeiraException(15);
//		} catch (MinhaPrimeiraException e) {
//			System.out.println("Ocorreram erros no processamento.");
//			e.printStackTrace();
//		}
		
		try {
			testaHierarquiaDaPrimeiraException(16);
			testaHierarquiaDaPrimeiraException(5);
		} catch (Exception e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		try {
			testaHierarquiaDaPrimeiraException(27);
		} catch (Exception e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		
		try {
			testaTodasExceptions(5);
		} catch (MinhaPrimeiraException | MinhaSegundaException e) { //especifico java 7
			System.out.println(e.getClass().toString());
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
		try {
			testaTodasExceptions(52);
		} catch (MinhaPrimeiraException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		} catch (MinhaSegundaException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		try {
			testaTodasExceptions(45);
		} catch (Throwable e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		} finally {
			System.out.println("Bloco finally sempre � executado");
		}
		
	}
	
}
