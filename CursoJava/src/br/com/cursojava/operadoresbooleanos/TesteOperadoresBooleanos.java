package br.com.cursojava.operadoresbooleanos;

public class TesteOperadoresBooleanos {

	public static void main(String[] args) {
		boolean x, y, z, resultado;
		x = true;
		y = false;
		z = x;
		int a = 0,
		    b = 1;
		
		// && AND
		resultado = x || z;
		System.out.println(x || y);
		System.out.println(resultado);
		// || OR 
		resultado = x || z;
		System.out.println(x || y);
		System.out.println(resultado);
		// ! NOT
		resultado = !z;
		System.out.println(!y);
		System.out.println(resultado);
		// & AND (Compara as condi��es)
		System.out.println(a == b & ++a <= b);
		System.out.println(a);
		// | OR (Compara as condi��es
		a = 0;
		b = 1;
		System.out.println(a == --b | ++a <= b);
		System.out.println(a);
	}
	
}
