package br.com.cursojava.exercicio.ecommerce;

import java.util.List;

public class BancoProduto {

	private List<EstoqueProduto> estoqueProduto;

	public BancoProduto() {
		super();
	}

	public List<EstoqueProduto> getEstoqueProduto() {
		return estoqueProduto;
	}

	public void setProdutos(List<EstoqueProduto> estoqueProduto) {
		this.estoqueProduto = estoqueProduto;
	}
	
}
