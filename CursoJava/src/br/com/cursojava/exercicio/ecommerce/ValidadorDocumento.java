package br.com.cursojava.exercicio.ecommerce;

import br.com.cursojava.exercicio.ecommerce.exception.DocumentoInvalidoException;

public abstract class ValidadorDocumento {
	
	public abstract void validarDocumento(String documento) throws DocumentoInvalidoException;
	
	
	public static ValidadorDocumento getInstance(TipoDocumentoEnum tipoDocumento){
		ValidadorDocumento res = null;
		
		switch (tipoDocumento) {
		case RG:
			res = new ValidadorRG();
			break;

		case CPF:
			res = new ValidadorCPF();
			break;

		default:
			break;
		}
		
		return res;
	}

}
