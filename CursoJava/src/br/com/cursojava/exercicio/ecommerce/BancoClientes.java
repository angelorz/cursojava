package br.com.cursojava.exercicio.ecommerce;

import java.util.ArrayList;
import java.util.List;

import br.com.cursojava.exercicio.banco.exceptions.ClienteNaoEncontradoException;
import br.com.cursojava.exercicio.ecommerce.Cliente;

public class BancoClientes {

	private List<Cliente> clientes;

	public BancoClientes() {
		super();
		Cliente cliente1 = new Cliente();
		cliente1.setId(1);
		cliente1.setRg("2977269");
		cliente1.setCpf("51711518735");
		
		Cliente cliente2 = new Cliente();
		cliente2.setId(2);
		cliente2.setRg("403289440");
		cliente2.setCpf("47702119454");
		
		Cliente cliente3 = new Cliente();
		cliente3.setId(3);
		cliente3.setRg("418757896");
		cliente3.setCpf("61353214710");
		
		clientes = new ArrayList<Cliente>();
		clientes.add(cliente1);
		clientes.add(cliente2);
		clientes.add(cliente3);
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	} 
	
	public void cadastrarCliente (Cliente cliente){
		this.clientes.add(cliente);
	}
	
	public Cliente findCliente (Integer idCliente) throws ClienteNaoEncontradoException {
		Cliente res = null;
		for (Cliente cliente : clientes) {
			if (idCliente.equals(cliente.getId())) {
				res = cliente;
				break;
			}
		}
		
		if (null == res) {
			throw new ClienteNaoEncontradoException("N�o encontrei o cliente com ID: " + idCliente);
		}
		
		return res;
	}
	
}
