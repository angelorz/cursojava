package br.com.cursojava.exercicio.ecommerce;

public class EstoqueProduto {

	private Integer saldoEstoque;
	private Produto produto;

	public EstoqueProduto() {
		super();
	}

	public EstoqueProduto(Integer saldoEstoque, Produto produto) {
		super();
		this.saldoEstoque = saldoEstoque;
		this.setProduto(produto);
	}

	public Integer getSaldoEstoque() {
		return saldoEstoque;
	}

	public void setSaldoEstoque(Integer saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
	
}
