package br.com.cursojava.exercicio.ecommerce;

public class Cliente {

	private Integer id; 
	private String rg;
	private String cpf;

	// Metodos construtores 
	public Cliente() {
		super();
	}

	public Cliente(Integer id, String rg, String cpf) {
		super();
		this.id = id;
		this.rg = rg;
		this.cpf = cpf;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	

	
}
