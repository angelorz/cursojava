package br.com.cursojava.exercicio.ecommerce;

import br.com.cursojava.exercicio.ecommerce.exception.CEPInvalidoException;

public interface ValidadorCEP {

	public void validarCEP(String cep) throws CEPInvalidoException;
	
	
}
