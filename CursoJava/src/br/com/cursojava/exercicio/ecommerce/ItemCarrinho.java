package br.com.cursojava.exercicio.ecommerce;

public class ItemCarrinho {

	private Integer quantidade;
	private Float valor;

	public ItemCarrinho() {
		super();
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	} 
	
	
	
	
}
