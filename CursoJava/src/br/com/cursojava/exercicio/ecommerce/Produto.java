package br.com.cursojava.exercicio.ecommerce;

public class Produto {

	private Integer id;
	private String descricao;
	private Float valor;

	public Produto() {
		super();
	}

	public Produto(Integer id, String descricao, Float valor) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}
	
}
