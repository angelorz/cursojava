package br.com.cursojava.exercicio.ecommerce;

public class ItemCompra {

	private Integer quantidade;
	private Float valor;

	public ItemCompra() {
		super();
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	} 
	
	
	
	
}
