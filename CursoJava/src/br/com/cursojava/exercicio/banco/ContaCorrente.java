package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

import br.com.cursojava.exercicio.banco.exceptions.SaldoInsulficienteException;

public class ContaCorrente extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		return BigDecimal.ZERO;
	}

	@Override
	public BigDecimal getIR() {
		return BigDecimal.ZERO;	}

	@Override
	public void validarLancamento(Lancamento lancamento) throws SaldoInsulficienteException  {
		Boolean res = getSaldo().add(lancamento.getValor()).compareTo(BigDecimal.ZERO) >= 0;
		if (!res) {
				throw new SaldoInsulficienteException("O saldo nao e sulficiente");
		}
	}

}
