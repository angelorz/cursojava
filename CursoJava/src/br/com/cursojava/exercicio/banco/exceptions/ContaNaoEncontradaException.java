package br.com.cursojava.exercicio.banco.exceptions;

public class ContaNaoEncontradaException extends BancoException {

	   	public ContaNaoEncontradaException() {
		   		super();
		}
		
		public ContaNaoEncontradaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
				super(message, cause, enableSuppression, writableStackTrace);
		}
		
		public ContaNaoEncontradaException(String message, Throwable cause) {
				super(message, cause);
		}
		
		public ContaNaoEncontradaException(String message) {
				super(message);
		}
		
		public ContaNaoEncontradaException(Throwable cause) {
				super(cause);
		}
	
}
