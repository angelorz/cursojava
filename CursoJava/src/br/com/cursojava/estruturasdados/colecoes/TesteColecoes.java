package br.com.cursojava.estruturasdados.colecoes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class TesteColecoes {

	public static void main(String[] args) {
		
//      C�digo emite erro porque n�o tem verifica��o de convers�o de tipo		
//		List listaObjetos = new ArrayList<>();
//		listaObjetos.add("Teste");
//		listaObjetos.add("Teste 2");
//		listaObjetos.add(1l);
//		listaObjetos.add(new Person());
//		
//		for (Object currentObject : listaObjetos) {
//			String s = (String) currentObject;
//            System.out.println(s.trim());		
//		}
		
		List<String> listaStrings = new ArrayList<>();
		listaStrings.add("tomate");
		listaStrings.add("maca");
		listaStrings.add("abobora");
		listaStrings.add("melancia");
		listaStrings.add("abobrinha");
		listaStrings.add("limao");
		listaStrings.add("abobrinha");
//		listaStrings.add("1l"); Erro porque long n�o � do tipo string

		Collections.sort(listaStrings);
		Collections.reverse(listaStrings);
		Collections.shuffle(listaStrings);
		
		for (String currentString : listaStrings) {
			System.out.println(currentString);
		}
		
		Set<Integer> conjuntosInteiros = new HashSet<Integer>();
		conjuntosInteiros.add(1);
		conjuntosInteiros.add(1);
		conjuntosInteiros.add(2);
		conjuntosInteiros.add(3);
		conjuntosInteiros.add(4);
		conjuntosInteiros.add(5);
		conjuntosInteiros.add(5);
		
		System.out.println("Teste de set com generics");
		for (Integer currentInteger : conjuntosInteiros) {
			System.out.println(currentInteger);
		}

		Set<Integer> conjuntosInteirosOrdenados = new TreeSet<Integer>();
		conjuntosInteirosOrdenados.add(5);
		conjuntosInteirosOrdenados.add(3);
		conjuntosInteirosOrdenados.add(4);
		conjuntosInteirosOrdenados.add(2);
		conjuntosInteirosOrdenados.add(2);
		conjuntosInteirosOrdenados.add(9);
		conjuntosInteirosOrdenados.add(5);
		
		System.out.println("Teste de sorted set com generics");
		for (Integer currentInteger : conjuntosInteirosOrdenados) {
			System.out.println(currentInteger);
		}
		
	}
	
	
}
