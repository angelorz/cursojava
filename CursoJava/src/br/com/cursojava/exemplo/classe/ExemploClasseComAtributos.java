package br.com.cursojava.exemplo.classe;

public class ExemploClasseComAtributos {

	// modificador default 
	String cadeiaDeCaracteres; 
	
	// modificador public
	public Integer umNumero;
	
	// Modificador private
	private long umNumeroLong;
	
	public static void main(String[] args) {
		
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.umNumeroLong = 1l;
		
	}
}
