package br.com.cursojava.exemplo.classe;

import br.com.cursojava.exemplo.classe.abstrata.ExemploClasseAbstrata;

public class ExemploClasse {

	// Classe concreta, n�o possui metodo abstrato
	// todos os metodos s�o definidos dentro dela
	
	public static void main(String[] args) {
	
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.cadeiaDeCaracteres = "";
		exemploClasseComAtributos.umNumero = 0;
	    // n�o podemos acessar atributos private em outra classe
		
        // ExemploClasseAbstrata classeAbstrata = new ExemploClasseAbstrata();
		// Classe abstrata n�o pode ser instaciada, apenas extendida 
		
	}
	
}
