package br.com.cursojava.exemplo.testeinterface;

public interface InterfaceTeste {

	// n�o permite modificador private
	// Interface n�o tem atibutos, apenas metodos
	public  void metodoA();

	public  String metodoB();
	
	public  Integer[] metodoC();
	
	
	
	
}
