package br.com.cursojava;

public class CalculadoraImpl implements Calculadora {

	@Override
	public Integer add(Integer... numeros) {
		Integer res = null;
		if (null != numeros && numeros.length > 0){
			res = numeros[0];
			
			for (int i = 1; i < numeros.length; i++) {
				res += numeros[i];
			}
		}
		return res;
	}

	@Override
	public Integer divide(Integer... numeros) {
		Integer res = null;
		if (null != numeros && numeros.length > 0){
			res = numeros[0];
			
			for (int i = 1; i < numeros.length; i++) {
				res /= numeros[i];
			}
		}
		return res;
	}

	@Override
	public Integer minus(Integer... numeros) {
		Integer res = null;
		if (null != numeros && numeros.length > 0){
			res = numeros[0];
			
			for (int i = 1; i < numeros.length; i++) {
				res -= numeros[i];
			}
		}
		return res;
	}

	@Override
	public Integer multiply(Integer... numeros) {
		Integer res = null;
		if (null != numeros && numeros.length > 0){
			res = numeros[0];
			
			for (int i = 1; i < numeros.length; i++) {
				res *= numeros[i];
			}
		}
		return res;
	}

}
