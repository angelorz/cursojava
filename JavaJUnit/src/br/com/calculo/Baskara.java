package br.com.calculo;

public class Baskara {

	private float a;
	private float b;
	private float c;
	

	private float x1;
	private float x2;
	
	private float delta;

	public Baskara(float a, float b, float c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void perform(){
		delta = (b*b - 4*a*c);
		if (delta < 0){
			throw new RuntimeException("Delta negativo impossibilita o calculo das raizes reais de uma equa��o de segundo grau");
		}
		x1 = ((-1.f * b) - ((Double)Math.sqrt(((Float) delta).doubleValue())).floatValue()) / 2.f*a;
		x2 = ((-1.f * b) + ((Double)Math.sqrt(((Float) delta).doubleValue())).floatValue()) / 2.f*a;
	}
	
	public float getA() {
		return a;
	}

	public void setA(float a) {
		this.a = a;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	public float getC() {
		return c;
	}

	public void setC(float c) {
		this.c = c;
	}

	public float getX1() {
		return x1;
	}

	public float getX2() {
		return x2;
	}

	public float getDelta() {
		return delta;
	}

	
}
