package br.com.cursojava;

public interface CalculadoraComplexa {

	public Integer pow(Integer base, Integer... numeros);
	
	public Integer factorial(Integer base);
	
	public Integer fib(Integer pos);
	
}
